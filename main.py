from fastapi import FastAPI

from database import engine, Base, SessionLocal
from routes import submenu, dish, menu
from models import Menu, Submenu, Dish

app = FastAPI()
Base.metadata.create_all(engine)

app.include_router(router=menu.router, prefix='/api/v1/menus')
app.include_router(router=submenu.router, prefix='/api/v1/menus/{menu_id}/submenus')
app.include_router(router=dish.router, prefix='/api/v1/menus/{menu_id}/submenus/{submenu_id}/dishes')


@app.on_event("startup")
def startup():
    db = SessionLocal()
    db.query(Dish).delete()
    db.commit()
    db.query(Submenu).delete()
    db.commit()
    db.query(Menu).delete()
    db.commit()
    db.close()
