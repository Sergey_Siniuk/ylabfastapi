from fastapi import Depends, HTTPException, APIRouter
from starlette.status import HTTP_201_CREATED
from sqlalchemy.orm import Session
from typing import List

from schemas import Menu, MenuUpdate, MenuCreate
from database import get_db
import crud

router = APIRouter()


@router.get("/{menu_id}", response_model=Menu)
async def get_menu(menu_id: int, db: Session = Depends(get_db)):
    db_menu = crud.get_menu(db=db, menu_id=menu_id)
    if db_menu is None:
        raise HTTPException(status_code=404, detail="menu not found")
    return db_menu


@router.get("", response_model=List[Menu])
async def list_menu(db: Session = Depends(get_db)):
    return crud.list_menu(db=db)


@router.post("", response_model=Menu, status_code=HTTP_201_CREATED)
async def add_menu(menu: MenuCreate, db: Session = Depends(get_db)):
    db_menu = crud.post_menu(db=db, menu_title=menu.title)
    if db_menu:
        raise HTTPException(status_code=400, detail="menu already exists")
    return crud.add_menu(db=db, menu=menu)


@router.patch("/{menu_id}", response_model=Menu)
async def update_menu(menu_id: int, menu: MenuUpdate, db: Session = Depends(get_db)):
    db_menu = crud.get_menu(db=db, menu_id=menu_id)
    if not db_menu:
        raise HTTPException(status_code=404, detail="menu not found")
    db_menu.title = menu.title
    db_menu.description = menu.description
    return crud.update_menu(db=db, menu_id=menu_id)


@router.delete("/{menu_id}")
async def delete_menu(menu_id: int, db: Session = Depends(get_db)):
    db_menu = crud.delete_menu(db=db, menu_id=menu_id)
    if db_menu is None:
        raise HTTPException(status_code=404, detail="menu not found")
    return {"status": True, "message": "The menu has been deleted"}
