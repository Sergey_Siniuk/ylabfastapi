from fastapi import Depends, HTTPException, APIRouter
from starlette.status import HTTP_201_CREATED
from sqlalchemy.orm import Session
from typing import List

from schemas import Submenu, SubmenuUpdate, SubmenuCreate
from database import get_db
import crud

router = APIRouter()


@router.get("/{submenu_id}", response_model=Submenu)
async def get_submenu(submenu_id: int, db: Session = Depends(get_db)):
    db_submenu = crud.get_submenu(db=db, submenu_id=submenu_id)
    if db_submenu is None:
        raise HTTPException(status_code=404, detail="submenu not found")
    return db_submenu


@router.get("", response_model=List[Submenu])
async def list_submenu(menu_id: int, db: Session = Depends(get_db)):
    submenus = crud.list_submenu(db=db, menu_id=menu_id)
    return submenus


@router.post("", response_model=Submenu, status_code=HTTP_201_CREATED)
async def add_submenu(menu_id: int, submenu: SubmenuCreate, db: Session = Depends(get_db)):
    db_submenu = crud.post_submenu(db=db, submenu_title=submenu.title)
    if db_submenu:
        raise HTTPException(status_code=400, detail="submenu already exists")
    return crud.add_submenu(db=db, submenu=submenu, menu_id=menu_id)


@router.patch("/{submenu_id}", response_model=Submenu)
async def update_submenu(menu_id: int, submenu_id: int, submenu: SubmenuUpdate, db: Session = Depends(get_db)):
    db_submenu = crud.get_submenu(db=db, submenu_id=submenu_id)
    if not db_submenu:
        raise HTTPException(status_code=404, detail="submenu not found")
    db_submenu.title = submenu.title
    db_submenu.description = submenu.description
    return crud.update_submenu(db=db, submenu_id=submenu_id)


@router.delete("/{submenu_id}")
async def delete_submenu(menu_id: int, submenu_id: int, db: Session = Depends(get_db)):
    db_submenu = crud.delete_submenu(db=db, menu_id=menu_id, submenu_id=submenu_id)
    if db_submenu is None:
        raise HTTPException(status_code=404, detail="submenu not found")
    return {"status": True, "message": "The submenu has been deleted"}
