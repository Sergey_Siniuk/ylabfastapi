from fastapi import Depends, HTTPException, APIRouter
from starlette.status import HTTP_201_CREATED
from sqlalchemy.orm import Session
from typing import List

from schemas import Dish, DishCreate, DishUpdate
from database import get_db
import crud

router = APIRouter()


@router.get("/{dish_id}", response_model=Dish)
def get_dish(dish_id: int, db: Session = Depends(get_db)):
    db_dish = crud.get_dish(db=db, dish_id=dish_id)
    if db_dish is None:
        raise HTTPException(status_code=404, detail="dish not found")
    return db_dish


@router.get("", response_model=List[Dish])
async def list_dish(submenu_id: int, db: Session = Depends(get_db)):
    dishes = crud.list_dish(db=db, submenu_id=submenu_id)
    return dishes


@router.post("", response_model=Dish, status_code=HTTP_201_CREATED)
async def add_dish(menu_id: int, submenu_id: int, dish: DishCreate, db: Session = Depends(get_db)):
    db_dish = crud.post_dish(db=db, dish_title=dish.title)
    if db_dish:
        raise HTTPException(status_code=400, detail="dish already exists")
    return crud.add_dish(db=db, dish=dish, menu_id=menu_id, submenu_id=submenu_id)


@router.patch("/{dish_id}", response_model=Dish)
async def update_dish(dish_id: int, dish: DishUpdate, db: Session = Depends(get_db)):
    db_dish = crud.get_dish(db=db, dish_id=dish_id)
    if not db_dish:
        raise HTTPException(status_code=404, detail="dish not found")
    db_dish.title = dish.title
    db_dish.description = dish.description
    db_dish.price = dish.price
    return crud.update_dish(db=db, dish_id=dish_id)


@router.delete("/{dish_id}")
async def delete_dish(menu_id: int, submenu_id: int, dish_id: int, db: Session = Depends(get_db)):
    db_dish = crud.delete_dish(db=db, dish_id=dish_id, menu_id=menu_id, submenu_id=submenu_id)
    if db_dish is None:
        raise HTTPException(status_code=404, detail="dish not found")
    return {"status": True, "message": "The dish has been deleted"}
