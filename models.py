from sqlalchemy import Column, Integer, String, ForeignKey, Numeric
from sqlalchemy.orm import relationship

from database import Base


class BaseTable:
    __abstract__ = True
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(100), nullable=False, unique=True)
    description = Column(String)


class Menu(BaseTable, Base):
    __tablename__ = 'menu'

    submenus_count = Column(Integer, default=0)
    dishes_count = Column(Integer, default=0)
    submenus = relationship("Submenu", cascade="all, delete-orphan", back_populates='menus')


class Submenu(BaseTable, Base):
    __tablename__ = 'submenu'

    dishes_count = Column(Integer, default=0)
    menus = relationship("Menu", back_populates='submenus')
    menu_id = Column(Integer, ForeignKey('menu.id'))
    dish = relationship("Dish", cascade="all, delete-orphan", back_populates='submenu')


class Dish(BaseTable, Base):
    __tablename__ = 'dish'

    price = Column(Numeric(10, 2), nullable=False)
    submenu_id = Column(Integer, ForeignKey('submenu.id'))
    submenu = relationship('Submenu', back_populates='dish')
