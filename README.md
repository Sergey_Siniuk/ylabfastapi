Клонировать проект:
```
git clone https://gitlab.com/Sergey_Siniuk/ylabfastapi.git
```
Запуск локально:
```
Установка зависимостей:

sudo apt install python3.10-venv
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
Отредактировать .env (параметры базы - #local_app)
Параметры по умолчанию:
```
DB_DATABASE=menu_local
DB_HOST=localhost
DB_CONNECTION=postgresql
DB_PORT=5432
DB_USERNAME=user1
DB_PASSWORD=pas
```

Команды запуска:
```
Запуск API
uvicorn main:app --reload
Запуск TEST
SQLALCHEMY_SILENCE_UBER_WARNING=1 pytest -s -vv

```
```
```
Запуск Docker контейнера:

```
Запуск проекта:
docker-compose build
docker-compose up

Запуск тестов:
docker-compose -f docker-compose.tests.yml build
docker-compose -f docker-compose.tests.yml up

```
