from typing import Optional
from pydantic import BaseModel


class MenuBase(BaseModel):
    title: str
    description: Optional[str]


class Menu(MenuBase):
    id: str
    submenus_count: int
    dishes_count: int

    class Config:
        orm_mode = True


class MenuCreate(MenuBase):
    pass


class MenuUpdate(MenuBase):
    pass


class SubmenuBase(BaseModel):
    title: str
    description: Optional[str]


class Submenu(SubmenuBase):
    id: str
    dishes_count: int

    class Config:
        orm_mode = True


class SubmenuCreate(SubmenuBase):
    pass


class SubmenuUpdate(SubmenuBase):
    pass


class DishBase(BaseModel):
    title: str
    description: Optional[str]
    price: Optional[str]


class Dish(DishBase):
    id: str

    class Config:
        orm_mode = True


class DishCreate(DishBase):
    pass


class DishUpdate(DishBase):
    pass
