# Базовый образ для сборки
FROM python:3.10-slim

# Рабочая директория
WORKDIR .

# Запрещаем Python писать .pyc на диск
ENV PYTHONDONTWRITEBYTECODE 1
# Запрещаем Python буферизовать stdout и stderr
ENV PYTHONUNBUFFERED 1

COPY . .

RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt