import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database import get_db, DATABASE_URL
from main import app, Base

engine = create_engine(DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


@pytest.fixture(scope='session')
def drop_db():
    Base.metadata.drop_all(engine)


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)
