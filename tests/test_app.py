from tests.conftest import client
from starlette import status

pref = "/api/v1/menus"


def test_get_menu_list():
    responce = client.get(pref)
    assert responce.json() == []
    assert responce.status_code == status.HTTP_200_OK


def test_get_menu():
    response = client.get(f"{pref}/0")
    assert response.json() == {'detail': 'menu not found'}
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_create_menu():
    json_request = {
        "title": "My menu 1",
        "description": "My menu description 1",
        "submenus_count": 0,
        "dishes_count": 0,
    }
    responce = client.post(pref, json=json_request)
    assert responce.json() == {
        "id": "1",
        "title": "My menu 1",
        "description": "My menu description 1",
        "submenus_count": 0,
        'dishes_count': 0
    }
    assert responce.status_code == status.HTTP_201_CREATED


def test_get_menu_by_id():
    responce = client.get(f"{pref}/1")
    assert responce.json() == {
        "id": "1",
        "title": "My menu 1",
        "description": "My menu description 1",
        "submenus_count": 0,
        'dishes_count': 0
    }
    assert responce.status_code == status.HTTP_200_OK


def test_patch_menu():
    json_request = {
        "title": "My updated menu 1",
        "description": "My updated menu description 1"
    }
    responce = client.patch(f"{pref}/1", json=json_request)
    assert responce.json() == {
        "id": "1",
        "title": "My updated menu 1",
        "description": "My updated menu description 1",
        "submenus_count": 0,
        'dishes_count': 0
    }
    assert responce.status_code == status.HTTP_200_OK


def test_delete_menu():
    responce = client.delete(f"{pref}/1")
    assert responce.json() == {"status": True, "message": "The menu has been deleted"}
    assert responce.status_code == status.HTTP_200_OK


def test_menu_not_found():
    responce = client.get(f"{pref}/1")
    assert responce.json() == {"detail": "menu not found"}
    assert responce.status_code == status.HTTP_404_NOT_FOUND


def test_submenu_create_menu():
    json_request = {
        "title": "My menu 1",
        "description": "My menu description 1",
        "submenus_count": 0,
        "dishes_count": 0,
    }
    responce = client.post(pref, json=json_request)
    assert responce.json() == {
        "id": "2",
        "title": "My menu 1",
        "description": "My menu description 1",
        "submenus_count": 0,
        'dishes_count': 0
    }
    assert responce.status_code == status.HTTP_201_CREATED


def test_get_submenu_list():
    responce = client.get(f"{pref}/2/submenu")
    assert responce.json() == {'detail': 'Not Found'}
    assert responce.status_code == status.HTTP_404_NOT_FOUND


def test_create_submenu():
    json_request = {
        "title": "My submenu 1",
        "description": "My submenu description 1",
        "dishes_count": 0,
    }
    responce = client.post(f"{pref}/2/submenus", json=json_request)
    assert responce.json() == {
        "id": "1",
        "title": "My submenu 1",
        "description": "My submenu description 1",
        "dishes_count": 0,
    }
    assert responce.status_code == status.HTTP_201_CREATED


def test_patch_submenu():
    json_request = {
        "title": "My update submenu 1",
        "description": "My update submenu description 1"
    }
    responce = client.patch(f"{pref}/2/submenus/1", json=json_request)
    assert responce.json() == {
        "id": "1",
        "title": "My update submenu 1",
        "description": "My update submenu description 1",
        "dishes_count": 0,
    }
    assert responce.status_code == status.HTTP_200_OK


def test_create_submenu1():
    json_request = {
        "title": "My submenu 2",
        "description": "My submenu description 2",
        "dishes_count": 0,
    }
    responce = client.post(f"{pref}/2/submenus", json=json_request)
    assert responce.json() == {
        "id": "2",
        "title": "My submenu 2",
        "description": "My submenu description 2",
        "dishes_count": 0,
    }
    assert responce.status_code == status.HTTP_201_CREATED


def test_delete_submenu():
    responce = client.delete(f"{pref}/2/submenus/2")
    assert responce.json() == {
        "status": True,
        "message": "The submenu has been deleted"
    }
    assert responce.status_code == status.HTTP_200_OK


def test_submenu_not_fount():
    responce = client.get(f"{pref}/1/submenu/3")
    assert responce.json() == {"detail": 'Not Found'}
    assert responce.status_code == status.HTTP_404_NOT_FOUND


def test_get_list_dishes():
    responce = client.get(f"{pref}/1/submenus/0/dishes")
    assert responce.json() == []
    assert responce.status_code == status.HTTP_200_OK


def test_create_submenu3():
    json_request = {
        "title": "My submenu 3",
        "description": "My submenu description 3",
        "dishes_count": 0,
    }
    responce = client.post(f"{pref}/2/submenus", json=json_request)
    assert responce.json() == {
        "id": "3",
        "title": "My submenu 3",
        "description": "My submenu description 3",
        "dishes_count": 0,
    }
    assert responce.status_code == status.HTTP_201_CREATED


def test_create_dish():
    json_request = {
        "title": "My dish 1",
        "description": "My dish description 1",
        "price": 12.50
    }
    responce = client.post(f"{pref}/2/submenus/3/dishes", json=json_request)
    assert responce.json() == {
        "id": "1",
        "title": "My dish 1",
        "description": "My dish description 1",
        "price": "12.50"
    }
    assert responce.status_code == status.HTTP_201_CREATED


def test_get_dish():
    responce = client.get(f"{pref}/2/submenus/3/dishes/1")
    assert responce.json() == {
        "id": "1",
        "title": "My dish 1",
        "description": "My dish description 1",
        "price": "12.50"
    }
    assert responce.status_code == status.HTTP_200_OK


def test_patch_dish():
    json_request = {
        "title": "My update dish 1",
        "description": "My update dish description 1",
        "price": 14.50
    }
    responce = client.patch(f"{pref}/2/submenus/3/dishes/1", json=json_request)
    assert responce.json() == {
        "id": "1",
        "title": "My update dish 1",
        "description": "My update dish description 1",
        "price": "14.50"
    }
    assert responce.status_code == status.HTTP_200_OK


def test_delete_dish():
    responce = client.delete(f"{pref}/2/submenus/3/dishes/1")
    assert responce.json() == {
        "status": True,
        "message": "The dish has been deleted"}
    assert responce.status_code == status.HTTP_200_OK


def test_delete_submenu3():
    responce = client.delete(f"{pref}/2/submenus/3")
    assert responce.json() == {
        "status": True,
        "message": "The submenu has been deleted",
    }
    assert responce.status_code == status.HTTP_200_OK


def test_delete_menu2():
    responce = client.delete(f"{pref}/2/")
    assert responce.json() == {
        "status": True,
        "message": "The menu has been deleted",
    }
    assert responce.status_code == status.HTTP_200_OK


def test_delete_dish_not_fount():
    responce = client.delete(f"{pref}/2/submenus/3/dishes/4")
    assert responce.json() == {
        "detail": "dish not found"
    }
    assert responce.status_code == status.HTTP_404_NOT_FOUND


def test_delete_submenu_not_fount():
    responce = client.delete(f"{pref}/2/submenus/3")
    assert responce.json() == {
        "detail": "submenu not found"
    }
    assert responce.status_code == status.HTTP_404_NOT_FOUND


def test_delete_menu_not_fount():
    responce = client.delete(f"{pref}/2")
    assert responce.json() == {
        "detail": "menu not found"
    }
    assert responce.status_code == status.HTTP_404_NOT_FOUND


def test_drop_db(drop_db):
    pass
