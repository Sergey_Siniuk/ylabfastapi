from sqlalchemy.orm import Session

from models import Menu, Submenu, Dish
from schemas import MenuCreate, SubmenuCreate, DishCreate


def get_menu(db: Session, menu_id: int):
    return db.query(Menu).filter(Menu.id == menu_id).first()


def list_menu(db: Session):
    return db.query(Menu).all()


def post_menu(db: Session, menu_title: str):
    return db.query(Menu).filter(Menu.title == menu_title).first()


def add_menu(db: Session, menu: MenuCreate):
    db_menu = Menu(**menu.dict())
    db.add(db_menu)
    db.commit()
    db.refresh(db_menu)
    return db_menu


def update_menu(db: Session, menu_id: int):
    db.commit()
    return get_menu(db=db, menu_id=menu_id)


def delete_menu(db: Session, menu_id: int):
    db_menu = get_menu(db, menu_id)
    if db_menu is None:
        return None
    else:
        db.delete(db_menu)
        db.commit()
        return True


def get_submenu(db: Session, submenu_id: int):
    return db.query(Submenu).filter(Submenu.id == submenu_id).first()


def list_submenu(menu_id: int, db: Session):
    return db.query(Submenu).filter(Submenu.menu_id == menu_id).all()


def post_submenu(db: Session, submenu_title: str):
    return db.query(Submenu).filter(Submenu.title == submenu_title).first()


def add_submenu(db: Session, submenu: SubmenuCreate, menu_id: int):
    db_submenu = Submenu(**submenu.dict())
    db_submenu.menu_id = menu_id
    get_menu(db=db, menu_id=menu_id).submenus_count += 1
    db.add(db_submenu)
    db.commit()
    db.refresh(db_submenu)
    return db_submenu


def update_submenu(db: Session, submenu_id: int):
    db.commit()
    return get_submenu(db=db, submenu_id=submenu_id)


def delete_submenu(db: Session, menu_id: int, submenu_id: int):
    db_submenu = get_submenu(db, submenu_id)
    if db_submenu is None:
        return None
    else:
        db_menu = get_menu(db=db, menu_id=menu_id)
        db_menu.submenus_count -= 1
        db_menu.dishes_count -= db_submenu.dishes_count
        db.delete(db_submenu)
        db.commit()
        return True


def get_dish(db: Session, dish_id: int):
    return db.query(Dish).filter(Dish.id == dish_id).first()


def list_dish(submenu_id: int, db: Session):
    return db.query(Dish).filter(Dish.submenu_id == submenu_id).all()


def post_dish(db: Session, dish_title: str):
    return db.query(Dish).filter(Dish.title == dish_title).first()


def add_dish(db: Session, dish: DishCreate, menu_id: int, submenu_id: int):
    db_dish = Dish(**dish.dict())
    db_dish.submenu_id = submenu_id
    get_menu(db=db, menu_id=menu_id).dishes_count += 1
    get_submenu(db=db, submenu_id=submenu_id).dishes_count += 1
    db.add(db_dish)
    db.commit()
    db.refresh(db_dish)
    return db_dish


def update_dish(db: Session, dish_id: int):
    db.commit()
    return get_dish(db=db, dish_id=dish_id)


def delete_dish(db: Session, dish_id: int, menu_id: int, submenu_id: int):
    db_dish = get_dish(db, dish_id)
    if db_dish is None:
        return None
    else:
        get_menu(db=db, menu_id=menu_id).dishes_count -= 1
        get_submenu(db=db, submenu_id=submenu_id).dishes_count -= 1
        db.delete(db_dish)
        db.commit()
        return True


def get_submenu_count(db: Session, menu_id):
    return db.query(Submenu).filter(Submenu.menu_id == menu_id).count()


def get_dishe_count(db: Session, submenu_id):
    return db.query(Dish).filter(Dish.submenu_id == submenu_id).count()
